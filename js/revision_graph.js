const legendOpenBtn = document.querySelector('.RevisionGraph-open-btn');
const legend = document.querySelector('.RevisionGraph-legend');
const legendClose = document.querySelector('.RevisionGraph-close-btn');
legendOpenBtn.onclick = () => {
  legendOpenBtn.style.display = 'none';
  legend.style.display = 'block';
}
legendClose.onclick = () => {
  legend.style.display = 'none';
  legendOpenBtn.style.display = 'block';
}

var myTemplateConfig = {
  colors: ['#00c58e', '#0096db', '#ff6f40'],
  commit: {
    message: {
      displayHash: false
    }
  }
};
var myTemplate = new GitgraphJS.templateExtend( "metro", myTemplateConfig );
commits = drupalSettings.revision_graph.commits;
console.log(commits);
var commitConfig = {
  subject: "Alors c'est qui le papa ?",
  author: "Me <me@planee.fr>"
};

// Get the graph container HTML element.
const graphContainer = document.getElementById("graph-container");
const options = {
  author: 'me <he@example.com>',
  template: myTemplate
};
// Instantiate the graph.
const gitgraph = GitgraphJS.createGitgraph(graphContainer, options);

const initialBranch = drupalSettings.revision_graph.initialBranch;
// console.log(initialBranch);
// Simulate git commands with Gitgraph API.
const master = gitgraph.branch(initialBranch);
var branches = new Array();
branches[initialBranch] = gitgraph.branch(initialBranch);
commits.forEach(element => {
  branches[element.branch].commit(element);
  if (element.tag) {
    branches[element.branch].tag(element.tag);
  }
  if (element.spawn_branches) {
    element.spawn_branches.forEach(b => {
      branches[b] = gitgraph.branch(b);
    });
  }
});
