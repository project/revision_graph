<?php

namespace Drupal\revision_graph\Controller;

use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Drupal\node\Controller\NodeController;
use Drupal\node\NodeInterface;
use Drupal\revision_graph\RevisionGraphStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Node routes.
 */
class RevisionGraphNodeController extends NodeController {

  /**
   * The default database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(DateFormatterInterface $date_formatter, RendererInterface $renderer, EntityRepositoryInterface $entity_repository = NULL, Connection $connection, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($date_formatter, $renderer, $entity_repository);
    $this->connection = $connection;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('date.formatter'),
      $container->get('renderer'),
      $container->get('entity.repository'),
      $container->get('database'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function revisionOverview(NodeInterface $node) {
    $commits = $this->revisionOverviewAsFakeCommits($node);

    return [
      '#theme' => 'revision_graph',
      '#attached' => [
        'library' => 'revision_graph/revision_graph',
        'drupalSettings' => [
          'revision_graph' => [
            'commits' => $commits,
            'initialBranch' => !empty($commits[0]['branch']) ? $commits[0]['branch'] : '',
          ],
        ],
      ],
    ];
  }

  /**
   * Load revision graph data from storage.
   *
   * @param Drupal\node\NodeInterface $node
   *   Node for which we need to load the revision graph.
   *
   * @return array|array[]
   *   List of revisions.
   */
  private function loadGraphData(NodeInterface $node) {
    $result = $this->connection->query('SELECT * FROM {revision_graph} WHERE primary_id = :entity_id', [':entity_id' => $node->id()])->fetchAll();
    $graph = [
      'map' => [],
      'branches' => [],
    ];

    foreach ($result as $r) {
      $graph['map'][$r->version_id] = $r->branch;

      if (!isset($graph['branches'][$r->branch])) {
        $graph['branches'][$r->branch] = $r->parent_version_id;
      }
    }

    return $graph;
  }

  /**
   * Load revision data as git commits.
   *
   * @param Drupal\node\NodeInterface $node
   *   Node for which we need to load the revision graph.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   *
   * @return array
   *   Revision data as list of commits.
   */
  private function revisionOverviewAsFakeCommits(NodeInterface $node) {
    $graph_data = $this->loadGraphData($node);
    $branches = $graph_data['branches'];
    $graph_data = $graph_data['map'];

    $commits = [];
    $langcode = $node->language()->getId();
    $node_storage = $this->entityTypeManager->getStorage('node');
    $default_revision = $node->getRevisionId();

    foreach ($this->revisionIds($node) as $vid) {
      $branches_to_be_spawned = [];

      foreach ($branches as $branch_name => $branch_spawn_vid) {
        if ($branch_spawn_vid == $vid) {
          $branches_to_be_spawned[] = $branch_name;
        }
      }
      $commit = [
        'author' => '',
        'subject' => '',
        'style' => ['dot' => ['font' => '8pt courier']],
        'branch' => $graph_data[$vid] ?? RevisionGraphStorage::UNTRACKED_BANCH_NAME,
        'spawn_branches' => $branches_to_be_spawned,
        'name' => $vid,
        'dotText' => $vid,
      ];
      /**
       * @var \Drupal\node\NodeInterface $revision
       */
      $revision = $node_storage->loadRevision($vid);
      $commit['author'] = $revision->getRevisionUser()->getAccountName() . ' <' . $revision->getRevisionUser()->getEmail() . '>';
      $date = $this->dateFormatter->format($revision->revision_timestamp->value, 'short');

      if ($vid !== $node->getRevisionId()) {
        $link = new Url('entity.node.revision', [
          'node' => $node->id(),
          'node_revision' => $vid,
        ]);
      }
      else {
        $link = $node->toLink($date);
      }
      $commit['date'] = $date;
      // Subject can be a good candidate for moderation state.
      $commit['subject'] = $date;
      $commit['body'] = strip_tags((string) $revision->revision_log->value);
      $commit['link'] = $link->toString();

      if (empty($commit['subject'])) {
        $commit['subject'] = '';
      }

      if ($vid === $default_revision) {
        $commit['tag'] = 'current';
      }
      $commits[] = $commit;
    }

    return array_reverse($commits);
  }

  /**
   * Loads revisions IDs by langcode.
   *
   * @param Drupal\node\NodeInterface $node
   *   The node.
   * @param string $langcode
   *   The langcode.
   *
   * @return mixed
   *   Array of revisions.
   */
  private function revisionIds(NodeInterface $node, $langcode = NULL) {
    $vids = [];
    $connection = $this->connection;
    if (isset($langcode)) {
      $result = $connection->query(
        'SELECT vid FROM {node_revision} WHERE nid=:nid AND langcode=:langcode ORDER BY vid DESC',
        [
          ':nid' => $node->id(),
          ':langcode' => $langcode,
        ]
      )->fetchAll();
    }
    else {
      $result = $connection->query(
        'SELECT vid FROM {node_revision} WHERE nid=:nid ORDER BY vid DESC',
        [
          ':nid' => $node->id(),
        ]
      )->fetchAll();
    }

    foreach ($result as $record) {
      $vids[] = $record->vid;
    }
    return $vids;
  }

}
