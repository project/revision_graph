<?php

namespace Drupal\revision_graph;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Path\CurrentPathStack;

/**
 * Storage handler for recording parent version id of a given revision.
 */
class RevisionGraphStorage {

  const UNTRACKED_BANCH_NAME = 'untracked';

  /**
   * The default database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The current path.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPath;

  /**
   * Creates a RevisionGraphStorage object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The default database connection.
   * @param \Drupal\Core\Path\CurrentPathStack $current_path
   *   The current path.
   */
  public function __construct(Connection $connection, CurrentPathStack $current_path) {
    $this->connection = $connection;
    $this->currentPath = $current_path;
  }

  /**
   * Initial revision graph data for a given entity.
   */
  public function initGraph(ContentEntityInterface $entity) {
    $this->connection->insert('revision_graph')->fields([
      'primary_id' => $entity->id(),
      'version_id' => $entity->getRevisionId(),
      'parent_version_id' => $entity->getRevisionId(),
      'type' => $entity->getEntityTypeId(),
      'branch' => $this->formatBranchName($entity->getRevisionId(), $entity->language()->getId()),
    ])->execute();
  }

  /**
   * Update revision graph data for a given entity.
   */
  public function updateGraph(ContentEntityInterface $entity) {
    $parent_version_id = $entity->getLoadedRevisionId();
    $branch = $this->getParentBranchName($parent_version_id);
    $result = $this->connection->query("SELECT * FROM {revision_graph} WHERE primary_id = :primary_id AND version_id = :version_id AND type = :type", [
      ':primary_id' => $entity->id(),
      ':version_id' => $entity->getRevisionId(),
      ':type' => $entity->getEntityTypeId(),
    ])->fetchAll();
    if (empty($result)) {
      $this->connection->insert('revision_graph')->fields([
        'primary_id' => $entity->id(),
        'version_id' => $entity->getRevisionId(),
        'parent_version_id' => $parent_version_id,
        'type' => $entity->getEntityTypeId(),
        'branch' => $this->formatBranchName($branch, $entity->language()->getId()),
      ])->execute();
    }
  }

  /**
   * Generate branch name based on parent branch and language.
   */
  protected function formatBranchName($parent_branch, $language_id): string {
    [$branch] = explode(':', $parent_branch);
    return implode(':', [$branch, $language_id]);
  }

  /**
   * Detect parent branch name for a given revision.
   */
  protected function getParentBranchName($version_id): string {
    $result = $this->connection->query("SELECT * FROM {revision_graph} WHERE version_id = :vid", [':vid' => $version_id])->fetchAll();
    $branch_name = self::UNTRACKED_BANCH_NAME;
    foreach ($result as $r) {
      $branch_name = $r->branch;
      break;
    }
    return $branch_name;
  }

}
