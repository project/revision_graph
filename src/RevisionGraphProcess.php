<?php

namespace Drupal\revision_graph;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;

/**
 * A computed property for processing parent vid.
 */
class RevisionGraphProcess extends FieldItemList {
  use ComputedItemListTrait {
    get as traitGet;
  }

  /**
   * Compute the values.
   */
  protected function computeValue() {
    $parent_revision_id = $this
      ->getParentRevisionId();
    // Do not store NULL values, in the case where an entity does not have a
    // moderation workflow associated with it, we do not create list items for
    // the computed field.
    if ($parent_revision_id) {

      // An entity can only have a single moderation state.
      $this->list[0] = $this
        ->createItem(0, $parent_revision_id);
    }
  }

  /**
   * Gets the Parent revision id fo a content entity revision.
   *
   * @return int|null
   *   The Parent revision ID linked to a content entity revision.
   */
  protected function getModerationStateId() {
    $default = NULL;
    $entity = $this
      ->getEntity();
    if (!$entity->isNew()
      ) {
      return 123;
    }
    return $default;
  }

}
